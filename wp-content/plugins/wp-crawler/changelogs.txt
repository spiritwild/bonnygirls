1.1.3 (August 20, 2016)
===
- Fix bug category in Schedule Crawl

1.1.2 (August 16, 2016)
===
- Fix bug Schedule Crawl
- Add New Feature: Auto translate to other language when crawl


1.1.1 (August 11, 2016)
===
- Add: Recurrence and Category fields into the Schedule Settings


1.1.0 (August 8, 2016)
===
- Add New Feature: Schedule Crawl


1.0.3 (Nov 6, 2015)
===
- Fix bug undefined index of single item url


1.0.2 (Nov 3, 2015)
===
- Improve alert messages when run crawler
- Add simulator HTTP headers for file_get_html function
- Fix insert feature images with the relative path


1.0.1 (Nov 2, 2015)
===
- Add support VK big image


1.0 (Nov 1, 2015)
===
- First Released
